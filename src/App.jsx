import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import WelcomeScreen from './WelcomeScreen';
import SurveyScreen from './SurveyScreen';
import ConfirmationScreen from './ConfirmationScreen';

function App() {
  return (
    
    <Router>
      <Routes>
        <Route path="/" element={<WelcomeScreen />} />
         <Route path="/survey" element={<SurveyScreen />} />
      <Route path="/confirmation" element={<ConfirmationScreen />} /> 
      </Routes>
    </Router>
  );
}

export default App;

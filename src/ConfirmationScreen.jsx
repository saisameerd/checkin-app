import React from 'react';
import './ConfirmationScreen.css'; // Importing the CSS file for styles
import illustration from "./assets/thanks.svg";
import logo from "./assets/woohoo.svg";

import { useNavigate } from "react-router-dom";




const ConfirmationScreen = ({ onDone }) => {
  const navigate = useNavigate();
  const handleBack = () => {
    navigate(-1); // Navigate back in history
  };
  return (
    <div className="confirmation-screen">
    <header className="survey-header">
        <span className="back-arrow" onClick={handleBack}> Back </span>
        <div className="logo"><img src={logo}></img></div>
      </header>
      <div className="content">
        <div className="illustration-logo">
         <img src={illustration}></img>
        </div>
        <h1 className="thank-you-message">THANK YOU!</h1>
        <p>We look forward to welcoming you to Postillion Hotels. Safe travels!</p>
        <button onClick={onDone} className="done-button">
          Done
        </button>
      </div>
    </div>
  );
};

export default ConfirmationScreen;

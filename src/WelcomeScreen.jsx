import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import logo from "./assets/woohoo.svg";

import "./WelcomeScreen.css"; // Importing the CSS file for styles

const WelcomeScreen = () => {
  const [name, setName] = useState("");
  const [selectedTime, setSelectedTime] = useState("");
  const navigate = useNavigate();

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleTimeSelection = (time) => {
    setSelectedTime(time);
  };

  const handleSubmit = () => {
    console.log("Submitted data:", { name, selectedTime });
    navigate("/survey");
    // Here you would handle the submission, e.g., sending data to the backend
  };

  return (
    <div className="welcome-screen">
      <div className="hotel">
</div>
      <div className="welcome-form">
        <div className="header">
          <div className="logo"><img src={logo}></img></div>
        </div>
        <div className="form">
        <div className="input-group">
        <label htmlFor="name" className="form-label">Name</label>
          <input
            type="text"
            placeholder="Name"
            id="name"
            value={name}
            onChange={handleNameChange}
            className="form-input"
          />
          </div>
          <div className="input-group">
          <label htmlFor="check-in" className="form-label">Check In Time</label>
            {[
              "10:00 am",
              "11:00 am",
              "12:00 pm",
              "01:00 pm",
              "02:00 pm",
              "03:00 pm",
            ].map((time) => (
              <button
                key={time}
                className={`time-option ${
                  selectedTime === time ? "selected" : ""
                }`}
                onClick={() => handleTimeSelection(time)}
              >
                {time}
              </button>
            ))}
          </div>
          <button onClick={handleSubmit} className="proceed-button">
            Proceed
          </button>
        </div>
      </div>
    </div>
      
  );
};

export default WelcomeScreen;

import React from 'react';
import './Question.css'; // Importing the CSS file for styles

const Question = ({ question, selectedOption, onOptionChange }) => {
  return (
    <div className="question">
      <div className="question-text">{question.text}</div>
      <div className="options-container">
        {question.options.map((option) => (
          <button
            key={option}
            className={`option-button ${selectedOption === option ? 'selected' : ''}`}
            onClick={() => onOptionChange(question.id, option)}
          >
            {option}
          </button>
        ))}
      </div>
    </div>
  );
};

export default Question;

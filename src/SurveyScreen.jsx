import React, { useState } from 'react';
import Question from './Question';
import './SurveyScreen.css'; // Importing the CSS file for styles
import { useNavigate } from "react-router-dom";
import logo from "./assets/woohoo.svg"

const questionsData = [
  {
    id: 1,
    text: '1. What is your preferred language?',
    options: ['English', 'Mandarin', 'Arabic', 'Dutch'],
  },
  {
    id: 2,
    text: '2. What is your ideal In-Room temperature?',
    options: ['19\u00b0C', '20\u00b0C', '21\u00b0C', '22\u00b0C' , '23\u00b0C','24\u00b0C'],
  },
  {
    id: 3,
    text: '3. What kind of meal cuisine you like?',
    options: ['Italian', 'French', 'Chinese', 'Thai', 'Indian','Lebanese' ],
  },
  {
    id: 4,
    text: '4. What kind of beverage do you generally prefer in evening?',
    options: ['Beer', 'Wine', 'Cocktails', 'Cold drinks' , 'Tea/Coffee'],
  },
  {
    id: 5,
    text: '5. What time do you generally like to have breakfast in hotel?',
    options: ['6 AM', '7 AM', '8 AM', '9 AM' , '10 AM'],
  },
  // ... add other questions in similar structure
];

const SurveyScreen = () => {
  const [responses, setResponses] = useState({});
  const navigate = useNavigate();


  const handleOptionChange = (questionId, option) => {
    setResponses({ ...responses, [questionId]: option });
  };

  const handleSubmit = async () => {
    const url = 'http://localhost:5000/api/check-in'; // replace with your actual endpoint
  
    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(responses),
      });
  
      if (response.ok) {
        const jsonResponse = await response.json();
        console.log('Success:', jsonResponse);
        // Handle success - perhaps redirect to a new page or show a success message
      } else {
        throw new Error('Network response was not ok.');
      }
      navigate("/confirmation");
    } catch (error) {
      console.error('Error:', error);
      // Handle errors - show error message to the user, etc.
    }
  };
  

  const handleBack = () => {
    navigate(-1); // Navigate back in history
  };

  const handleSkip = () => {
    navigate('/confirmation'); // Replace with your actual next route
  };

  return (
    <div className="survey-screen">
     <header className="survey-header">
        <span className="back-arrow" onClick={handleBack}> Back </span>
        <div className="logo"><img src={logo}></img></div>
        <button className="skip-button" onClick={handleSkip}>       Skip </button>
      </header>
    <div className="info">
        <h2>May I Know You Better?</h2>
        <p>Sharing a bit more about yourself will help Postillion Hotels to prepare everything for your arrival. 😊</p>
    </div>
      <div className="questions-container">
        {questionsData.map((question) => (
          <Question
            key={question.id}
            question={question}
            selectedOption={responses[question.id]}
            onOptionChange={handleOptionChange}
          />
        ))}
      </div>
      <button onClick={handleSubmit} className="submit-button">
        Confirm
      </button>
    </div>
  );
};

export default SurveyScreen;

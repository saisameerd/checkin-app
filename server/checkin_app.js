const express = require('express');
const bodyParser = require('body-parser');
const { Pool } = require('pg');

const app = express();
const port = 3000;

// PostgreSQL pool connection
const pool = new Pool({
  user: 'your_username',
  host: 'localhost',
  database: 'your_database',
  password: 'your_password',
  port: 5432,
});

app.use(bodyParser.json());

// Endpoint to receive survey data
app.post('/submit-survey', async (req, res) => {
  try {
    const { language, temperature, cuisine } = req.body;
    const result = await pool.query(
      'INSERT INTO survey_responses(language, temperature, cuisine) VALUES($1, $2, $3) RETURNING *',
      [language, temperature, cuisine]
    );
    res.status(201).json(result.rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).send('Server error');
  }
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

async getCheckinDetails(req, res) {
    try {
      const cachedData = await cacheService.getCache(req.domainId, `checkinApp`);
      if (cachedData) {
        reqRes.responseHandler(
          req,
          'Checkin Details',
          cachedData,
          res
        );
        return res.end();
      }
      const data = await dashboard.getCheckinDetails(req.domainId);
      cacheService.setCache(req.domainId, `checkinApp`, data)
      reqRes.responseHandler(
        req,
        "Checking Details",
        data,
        res
      );
      return res.end();
    } catch (err) {
      reqRes.httpErrorHandler(req, err, res);
      return res.end();
    }
  }

